Forked from [Optimum Tech](https://www.dropbox.com/scl/fo/2u8tmqwt5yv5jtultai7y/h?rlkey=tv2722h7s6wnxhumw7bgi5wzh&dl=0)
See this [video](https://www.youtube.com/watch?v=Ao2lNd7jCb8) for details

My goal is to turn this into a wall mountable Mini-ITX case

Original readme.txt:
```
Hey! I actually recommend you don't print this yet.
I'll be making some serious improvements to it over the next couple of weeks and I'll make an updated video then.

For now, feel free to take a look at the 3D files and complete model in Fusion 360.

The current beta build uses: 
- 13 M3x5 countersunk 
- 11 M2.5x5 countersunk
- 10 M2.5x4 countersunk
```